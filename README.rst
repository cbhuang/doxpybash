=======================
doxpybash v0.0.0 alpha
=======================

**STILL VERY NAIVE!** Creative constructs are welcomed!

A Pythonic-Style Doxygen INPUT_FILTER for BASH Project Documentations.
Currently linux only.


Features
==========

* File Documentation (Python 3 Docstring at file beginning)
* Function Documentation
* Global Variable Documentation


Setup
==========

Prequisites
---------------

Please install the equivalent of the below debian packages for your linux distribution.

.. code-block:: bash

  # required
  $ sudo aptitude install doxygen python3

  # optional
  $ sudo aptitude install graphviz

Download
------------

.. code-block:: bash

  $ git clone https://gitlab.com/cbhuang/doxpybash


Usage
=======

1. Format comments in your bash files as follows.

  1-1. Place "pseudo-docstring" at the beginning of file or right after functions as if you are writing python 3:

  ::

    #"""! the first line begins with triple quotes plus an !
    # arbitrary lines begins with #
    # line 2
    # line 3
    #""" (final line contains no extra text)

  1-2. Place ``##`` for global variable documentation:

  ::

    ## Here explains a global variable
    var=value


2. ``cd`` to your project directory

3. Generate doxygen settings: ``doxygen -g dox``

4. Tune ``dox`` file as usual. The key differences are listed as follows. (Example: ``example/ex1/dox``)

::

  INPUT_FILTER = "/absolute/path/to/doxpybash.py"
  FILE_PATTERNS = *.sh
  EXTENSION_MAPPING = sh=Python
  EXTRACT_ALL = YES
  EXTRACT_STATIC = YES


5. Generate project documentation: ``doxygen dox``

6. View your results:

.. code-block:: bash

  $ cd html
  $ python -m SimpleHTTPServer
  # then go to your browser and see http://localhost:8000


Example Outcome
===============

Source
------
example/ex1/example.sh

.. image:: img/example_sh.png
   :height: 500


Variables(global)
---------------------
.. image:: img/package_namespace.png
   :height: 500


Functions
----------------
.. image:: img/function.png
   :height: 500


TODO
=====
* Add test cases
* Move file and state variables into class(?)
* recognize array variables (?)
* use in kcauto project (?)
