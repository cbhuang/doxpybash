#!/usr/bin/python3
#"""! @package doxpybash
# @brief    convert "faked docstring" blocks into python-style doxygen blocks
# @date     2017-10-04
# @author   Chuan-Bin "Bill" Huang
# @copyright  FreeBSD
#
# @section doxpybash_usage Usage
#
#     1. Generate and set up your doxygen config file as usual.
#     2. Set INPUT_FILTER = "/path/to/this/script"
#        e.g. INPUT_FILTER = "/opt/doxpybash/doxpybash.py"
#     3. Run `doxygen` to see whether the ## blocks are parsed.
#
# @section doxpybash_example Example
#
#   Input:
#      NOTE: The triple quotes are connected. Spaces here are just to avoid
#      interrupting script demo.
#      \#" " " block
#      \# for
#      \# doxygen
#      \#" " " this line won't appear
#
#   Expected Output:
#      " " "block
#      for
#      doxygen
#      " " "
#
# @section doxpybash_functions "Parsing Functions"
#
#     Function definions are "faked out" like this:
#
#     myfunc() {            def myfunc():
#       <docstr>               <docstr>
#       ....           =>      pass
#     }
#
#
#"""

import re
import argparse

class DoxPybash():
    #"""! @brief Bash to Python(fake) Text Processor Class
    #
    # Attributes:
    #
    #     _str_begin (string): block beginning pattern
    #     _str_mid (string): block continuation pattern
    #
    #"""

    # basic string pattern
    _str_begin = '#"""!'  # undocumented feature!
    _str_mid = '#'
    _str_end = '#"""'

    # substitution pattern
    _str_sub_begin = '"""!'
    _str_sub_mid = ""
    _str_sub_end = '"""'

    def __init__(self):
        #"""! @brief doxpybash Constructor
        # @retval _re_begin    The comment block prefix intended for doxygen use
        # @retval _str_begin   Doxygen block start
        # @retval _str_mid     Doxygen block continuation
        # @retval _str_end     Doxygen block close
        #"""

        # match pattern
        self._re_begin = re.compile('^\s*' + self._str_begin)
        self._re_mid = re.compile('^\s*' + self._str_mid)
        self._re_end = re.compile('^\s*' + self._str_end)

        # function regex pattern
        self._funcReObj = [None, None]

        self._funcReObj[0] = re.compile(r"""
                ^
                \s*function\s*    # function keyword
                (?P<fn>[^\(\s]+)  # function name
                (\s*\(\))?        # optional parentheses
            """, re.VERBOSE)

        self._funcReObj[1] = re.compile(r"""
                ^
                \s*               # no function keyword
                (?P<fn>[^\(\s]+)  # function name
                \(\)              # parentheses
            """, re.VERBOSE)


    def determine_state(self, st_prevline, line):
        #"""! @brief determine state of current line
        # @param st_prevline  previous state
        # @param line         line to be parsed
        # @retval 0           irrevalent line
        # @retval 1           within doxy-block
        # @retval 2           within doxy-block (last line)
        #"""

        if st_prevline in (0, 2):
            # enter "doxy-block"
            if self._re_begin.match(line) is not None:
                return 1
            # irrelavent line
            else:
                return 0
        elif st_prevline == 1:
            # block termination
            if self._re_end.match(line) is not None:
                return 2
            # block continuation (NOTE: cannot be placed first!)
            elif self._re_mid.match(line) is not None:
                return 1
            else:
                raise ValueError("Bad data: " + line)
        else:
            raise ValueError("Bad st_prevline: " + str(st_prevline))


    def parse_func(self, line):
        #"""! @brief parse functions
        #"""

        # return formatted function name
        for o in self._funcReObj:
            matchObj = o.match(line)
            if matchObj is not None:
                return "def {0}():".format(matchObj.group("fn"))
        # if not found, do nothing
        else:
            return None


    def action(self, st_thisline, st_prevline, line):
        #"""! perform substitution based on states of lines
        #
        #"""

        # irrelavent line => find function definition
        if st_thisline == 0:
            # parse function name
            if self.parse_func(line) is not None:
                print(self.parse_func(line))
            # kill singleton brackets
            elif re.match('^[\{\}]', line) is not None:
                print("")
            else:
                print(line, end="")
        # within block
        elif st_thisline == 1:
            # beginning
            if st_prevline == 0:
                print(re.sub(self._str_begin, self._str_sub_begin, line, 1), end="")
            # mid
            elif st_prevline == 1:
                print(re.sub(self._str_mid, self._str_sub_mid, line, 1), end="")
            else:
                raise ValueError("Bad st_prevline: " + str(st_prevline))
        # ending line
        elif st_thisline == 2:
            print(re.sub(self._str_end, self._str_sub_end, line, 1), end="")
        # wrong
        else:
            raise ValueError("Bad st_thisline: " + str(st_thisline))



################
# Main Program
################

#"""! @brief Example Brief of Global Parameters
# @var    args.filename   File to be processed (relative path)
# @def    st_thisline     state code of this line
# @static st_prevline     state code of previous line
#"""

if __name__ == "__main__":

    # get filename
    parser = argparse.ArgumentParser()
    parser.add_argument("filename",
                        nargs='?',  # make this arg optional
                        default="doxpybash.py",
                        help="file to be processed")
    args = parser.parse_args()
    if args.filename == "doxpybash.py":
        print("\n[INFO] No file specified. Processing this script as an example.\n")

    # go
    doxpybash = DoxPybash()
    with open(args.filename) as f:

        # init states: out of block = 0, in block = 1
        st_thisline = 0
        st_prevline = 0

        for line in f:
            # backshift state
            st_prevline = st_thisline
            st_thisline = doxpybash.determine_state(st_prevline, line)

            # process line
            doxpybash.action(st_thisline, st_prevline, line)

    # ensure the output ends with a newline
    print("")
