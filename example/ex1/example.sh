#!/bin/bash
#"""! @package example
# @brief      Here comes the brief of this example package.
# @author     Chuan-Bil "Bill" Huang
# @date       2017-10-04
# @copyright  FreeBSD
#
# @section Usage
#          Here illustrates the usage of this example package.
#"""

# Non-doxygen comments (should not be processed)
source whatever.sh

## Here explains numeric global var a
a=1

## Here explains string global var s
s=str


# Non-doxygen comments (should not be processed)
myfunc() {  # Non-doxygen comments (should not be processed)
  #"""! @brief  Here is the brief of this example function
  # @param  $1  Here explains parameter1
  # @param  $2  Here explains parameter2
  # @return  Here explains whatever the function echoes
  # @retval val1   Here explains val1
  # @retval val2   Here explains val2
  #"""
  echo "$1" "$2"
}
# Non-doxygen comments (should not be processed)
